Rails.application.routes.draw do
  root 'home#show'
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'api/v1/sessions',
               registrations: 'api/v1/registrations'
             }
    scope module: 'api' do
    namespace :v1 , defaults: { format: :json } do
      # mount_devise_token_auth_for 'User', at: 'auth'
      # devise_for :users
      get 'get_chain_limit' => 'global_settings#get_chain_limit'
      post 'set_chain_limit' => 'global_settings#set_chain_limit'

      resources :users, only: [:index, :show] do
        collection do
          get 'generate_invitation'
          get 'contact_list'
          post 'accept_invition'
          patch 'update_password'
          patch 'edit_contact_label'
          post 'get_contact_label'
        end
        member do
          get 'get_invitation_limit'
          post 'set_invitation_limit'
          get 'get_nickname'
          put 'set_nickname'
          get 'item_list'
        end
      end
      resources :invitations, only: [:index, :update] do
        put 'set_user_type'
        put 'set_note_label'
        collection do
          patch 'user_info'
        end
      end
      resources :categories,                          only: [:index]
      resources :grades,                              only: [:index]
      resources :item_names,                          only: [:index]
      resources :items,                               only: [:index, :create, :update, :destroy]
      resources :user_category_prices,                only: [:create]
      resources :user_relationship_prices,            only: [:create]
      resources :user_relationship_request_prices,    only: [:create]
      post 'verify_invitation_code' => 'home#verify_invitation_code'
      get 'available_user_type' => 'home#available_user_type'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
