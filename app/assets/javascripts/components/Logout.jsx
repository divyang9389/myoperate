var Logout = createReactClass({

  handleLogout(e) {
  	var that= this;
    $.ajax({
      type: "DELETE",
      url: API_URL + "/logout",
      dataType: "json",
      success: function(data, textStatus, jqXHR){
   		that.props.changePage("login");
      },
    })
  },

  render: function() {
    return (
      <button onClick={this.handleLogout}>Sign Out</button>
    );
  }
});
