var Edit = createReactClass({
  getInitialState: function() {
    return {
      generatedCode: '',
    };
  },

  generateCode(e){
    var that = this;
    e.preventDefault();
    $.ajax({
      type: "GET",
      url: API_URL + "/v1/users/generate_invitation",
      dataType: "json",
      error: function (xhr, status, error) {
        that.setState({generatedCode: JSON.parse(xhr.responseText).message })
      },
      success: function (res) {
        that.setState({generatedCode: res.invitation_code})
      },
    });
  },

  render: function() {
    return (
      <div>
        <h2>My Account</h2>
        <h3>Invite Your Friends</h3>
        <form>
          <button onClick={this.generateCode}>Generate Code</button>  {this.state.generatedCode}
        </form>
        <Logout changePage={this.props.changePage}/>
      </div>
    );
  }
});
