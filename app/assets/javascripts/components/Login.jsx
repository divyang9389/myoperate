var Login = createReactClass({
  getInitialState: function() {
    return {
      errorMessage: '',
      inviteCode: ''
    };
  },

  handleLogin(e) {
    e.preventDefault();
    var that = this;
    var userInfo = {
      user_name: document.getElementById("user_name").value,
      password: document.getElementById("password").value
    }
    $.ajax({
      type: "POST",
      url: API_URL + "/login",
      dataType: "json",
      data: userInfo,
      error:  function(xhr, status, error) {
        that.updateLoginError(JSON.parse(xhr.responseText).message);
      },
      success: function(data, textStatus, jqXHR){
        that.setState({errorMessage: ''})
        console.log(jqXHR.getResponseHeader('access-token'))
        that.props.changePage("edit");
      },
    })
  },
  handleVerifyInviteCode(e) {
    var that = this;
    $.ajax({
      type: "POST",
      url: API_URL + "/v1/verify_invitation_code",
      dataType: "json",
      data: {
        invitation_token: that.state.inviteCode
      },
      error:  function(xhr, status, error) {
        that.updateLoginError(JSON.parse(xhr.responseText).message);
      },
      success: function(data, textStatus, jqXHR){
        console.log(jqXHR)
        // that.props.setInvitationCode(jqXHR) 
        that.props.changePage("signup")
      },
    })
  },
  updateLoginError(str) {
    this.setState({
      errorMessage: str
    });
  },

  render() {
    return (
      <div>
        <h2>Login</h2>
        <form>
          <input id="user_name" placeholder="Username"/>
          <input id="password" placeholder="Password"/>
          <button onClick={this.handleLogin}>Submit</button>
        </form>
        <p>{this.state.errorMessage}</p>
        <input id="invite_code" placeholder="Invite Code" onChange={(event)=>this.setState({inviteCode: event.target.value})}/>
        <button onClick={(e) => this.handleVerifyInviteCode(e)}>Submit</button>
      </div>
    );
  }
});
