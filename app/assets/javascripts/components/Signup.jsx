var Signup = createReactClass({
  getInitialState: function() {
    return {
      errorMessage: ''
    };
  },

  handleSignup(e) {
    e.preventDefault();
    var that = this;
    var userInfo = {
      user_name: document.getElementById("user_name").value,
      password: document.getElementById("password").value,
      password_confirmation: document.getElementById("password_confirmation").value,
      invitation_code: that.props.inviteCode
      // confirm_success_url: "http://192.168.0.6:3009"
    }
    $.ajax({
      type: "POST",
      url: API_URL + "/signup",
      dataType: "json",
      data: {"user" : userInfo},
      error:  function(xhr, status, error) {
        console.log(xhr.responseText)
        that.updateLoginError(JSON.parse(xhr.responseText).message);
      },
      success: function (res) {
        that.setState({errorMessage: ''})
        that.props.changePage("login");
      },
    });
  },

  updateLoginError(str) {
    this.setState({
      errorMessage: str
    });
  },

  render: function() {
    return (
      <div>
        <h2>Signup</h2>
        <form>
          <input id="user_name" placeholder="Username"/>
          <input id="password" placeholder="Password"/>
          <input id="password_confirmation" placeholder="Retype Password"/>
          <button onClick={this.handleSignup}>Submit</button>
        </form>
        <p>{this.state.errorMessage}</p>
        <button onClick={() => this.props.changePage("login")}>Login!</button>
      </div>
    );
  }
});
