class HomeController < ApplicationController
  def show
    render template: "landing.html.erb"
  end
end