module Api::V1
  class ItemsController < ApiController
    before_action :authenticate_user!, :check_user_type
    before_action :set_item, only: [:update, :destroy]
    # This method will return all item which posted by contact of current user
    # url : /v1/items
    # method : GET
    def index
      relationships = Relationship.where("user_id = #{current_user.id} OR friend_id = #{current_user.id}")
      if relationships.count > 0
        @user = relationships.pluck(:user_id, :friend_id).flatten!.uniq
        @items = Item.where("user_id IN (?)", @user)
      end
      render json: @items, status: 200
    end
    #This method will create item
    #url /v1/items
    #method : POST
    #parameter
    def create
      @item = current_user.items.new(item_params)
      if @item.save
        render json: @item, status: 200
      else
        render :json=> @item.errors, :status=>422
      end
    end

    def update
      if @item.update(item_params)
        render json: @item, status: 200
      else
        render :json=> @item.errors, :status=>422
      end
    end

    def destroy
      @item.destroy!
      render json: {
        message: "Item delete successfully."
      }, staus: 200
    end

    private
    def set_item
      @item = current_user.items.find_by(id: params[:id])
      unless @item
        render json: {
          message: "You are not authorised to access."
        }, status: 422
      end
    end

    def item_params
      params.require(:item).permit(:user_id,:quantity, :category_id,:item_name_id, :name, :grade_id, :price, :date_available, :item_unit_id, :unit)
    end

    def check_user_type
      # unless (current_user.user_groups.pluck(:group_label).include?("producer") || current_user.is_admin?)
      #   render json: {
      #     message: "You are not authorised to access."
      #   }, status: 422
      # end
    end
  end
end