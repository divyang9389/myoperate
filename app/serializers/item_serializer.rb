class ItemSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :quantity, :category_id, :name, :grade_id, :price, :item_unit_id, :item_name_id, :date_available, :unit_name

  def unit_name
    object.item_unit.unit_name
  end
end
