FactoryBot.define do
  factory :request_list_relationship_status do
    request_list { nil }
    relationship { nil }
  end
end
