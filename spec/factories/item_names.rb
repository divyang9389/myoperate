FactoryBot.define do
  factory :item_name do
    name { "MyString" }
    category { nil }
    description { "MyText" }
  end
end
