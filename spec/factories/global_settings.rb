FactoryBot.define do
  factory :global_setting do
    chain_limit { 1 }
  end
end
